
inline LookaheadSet::LookaheadSet(Element const **begin, Element const **end)
:
    FirstSet(begin, end)
{}

inline FirstSet &LookaheadSet::firstSet()
{
    return *this;
}

inline bool LookaheadSet::operator>=(Symbol const *symbol) const
{
    return find(symbol) != end();
}

inline bool LookaheadSet::operator<(LookaheadSet const &other) const
{
    return not (*this >= other);
}

inline bool LookaheadSet::hasEOF() const
{
    return d_EOF == e_withEOF;
}        

inline void LookaheadSet::rmEOF()
{
    d_EOF = e_withoutEOF;
}        

inline bool LookaheadSet::empty() const
{
    return d_EOF == e_withoutEOF && FirstSet::empty();
}

inline size_t LookaheadSet::fullSize() const
{
    return size() + (d_EOF == e_withEOF);
}
