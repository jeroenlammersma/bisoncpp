#include "parser.ih"

Parser::STYPE_ Parser::setPrecedence(size_t type)
{
    if (d_rules.empty())
    {
        emsg << "`%prec " << d_matched << 
                "': `%prec' requires a non-empty production"  << endl;
        return STYPE_{};
    }

    Symbol *sp = 0;     // to prevent `sp uninitialized' warning by the
                        // compiler 

    switch (type)       // the %prec argument: either a named operator or
    {                   //                     a char-operator    
        case IDENTIFIER:
            sp = d_symtab.lookup(d_matched);
        break;

        case QUOTE:
            sp = d_symtab.lookup(d_scanner.canonicalQuote());
        break;
    }        

        // set the precedence of the current production rule.
        //
    if (sp && sp->isTerminal())
        d_rules.setPrecedence(Terminal::downcast(sp));
    else
        emsg << "`%prec " << d_matched << "': `" << d_matched << 
                "' must be a declared terminal token" << endl;

    return STYPE_{};
}



