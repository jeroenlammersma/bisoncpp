#include "parser.ih"

namespace
{
    char const *names[] = { "STYPE_", "END_TYPE_" };
    unordered_set<string> reserved{ names, names + size(names) };
}

void Parser::addPolymorphic(string const &tag, string const &typeSpec) 
{
    if (d_semType != POLYMORPHIC)
        return;

    if (auto iter = reserved.find(tag); iter != reserved.end())
        emsg << "Polymorphic type cannot be " << *iter << endl;
    else if (d_polymorphic.find(tag) != d_polymorphic.end())
        emsg << "Polymorphic semantic tag `" << tag << "' multiply defined" <<
                endl;
    else 
        d_polymorphic[tag] = typeSpec; 

    // d_polymorphic["END_TAG_"] = "<IUO>" is set in 'expectRules'
}
