#ifndef INCLUDED_TOKENS_
#define INCLUDED_TOKENS_

struct Tokens
{
    // Symbolic tokens:
    enum Tokens_
    {
        BASECLASS_HEADER = 257,
        BASECLASS_PREINCLUDE,
        BLOCK,
        CLASS_HEADER,
        CLASS_NAME,
        CONSTRUCTOR_CHECKS,
        DEBUGFLAG,
        DEFAULT_ACTIONS,
        ERROR_VERBOSE,
        EXPECT,
        FILENAMES,
        FLEX,
        IDENTIFIER,
        IMPLEMENTATION_HEADER,
        LEFT,
        LOCATIONSTRUCT,
        LSP_NEEDED,
        LTYPE,
        NAMESPACE,
        NEG_DOLLAR,
        NOLINES,
        NONASSOC,
        NUMBER,
        PARSEFUN_SOURCE,
        POLYMORPHIC,
        PREC,
        PRINT_TOKENS,
        QUOTE,
        REQUIRED,
        RIGHT,
        SCANNER,
        SCANNER_CLASS_NAME,
        SCANNER_MATCHED_TEXT_FUNCTION,
        SCANNER_TOKEN_FUNCTION,
        STACK_EXPANSION,
        START,
        STRING,
        STYPE,
        TARGET_DIRECTORY,
        TOKEN,
        TWO_PERCENTS,
        TYPE,
        UNION,
        WARN_TAGS,
        WEAK_TAGS,
        PROMPT,
        THREAD_SAFE,
        TOKEN_CLASS,
        TOKEN_NAMESPACE,
        TOKEN_PATH,
    };

};

#endif
