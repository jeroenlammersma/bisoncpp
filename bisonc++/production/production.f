inline void Production::setBlockLineNr(size_t lineNr)
{
    d_action.setLineNr(lineNr);
}

inline void Production::setLineNr(size_t lineNr)
{
    d_lineNr = lineNr;
}

inline std::string const &Production::fileName() const
{
    return s_fileName[d_nameIdx];
}

inline size_t Production::lineNr() const
{
    return d_lineNr;
}

inline bool Production::notUsed()
{
    return s_unused;
}

inline Symbol const *Production::rhs(size_t idx) const
{
    return vectorIdx(idx);
}

inline Symbol const *Production::lhs() const
{
    return d_nonTerminal;
}

inline size_t Production::nr() const
{
    return d_nr;
}

inline void Production::used() const
{
    d_used = true;
}

inline Block const &Production::action() const
{
    return d_action;
}

inline bool Production::hasAction() const
{
    return !d_action.empty();
}

inline bool Production::isEmpty() const
{
    return empty() && d_action.empty();
}

inline Terminal const *Production::precedence() const
{
    return d_precedence;
}

inline Symbol const &Production::operator[](size_t idx) const
{
    return *vectorIdx(idx);
}

inline void Production::setAction(Block const &block)
{
    d_action = block;
}
 
inline void Production::setStart(Production const *production)
{
    s_startProduction = production;
}

inline Production const *Production::start()
{
    return s_startProduction;
}

inline std::ostream &operator<<(std::ostream &out, Production const *prod)
{
    return prod->standard(out);
}

inline void Production::termToNonterm(Production *pPtr, 
                                      Symbol *terminal, Symbol *nonTerminal)
{
    std::replace(pPtr->begin(), pPtr->end(), terminal, nonTerminal);
}
