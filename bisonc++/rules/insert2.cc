#include "rules.ih"

NonTerminal *Rules::insert(NonTerminal *nonTerminal)
{
    d_empty = false;                        // this production is non-empty

    d_nonTerminal.push_back(nonTerminal);

    return d_nonTerminal.back();
}
