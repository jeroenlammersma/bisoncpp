
inline Rules::Rules()
:
    d_defaultAction(false),
    d_empty(true),
    d_currentRule(0),
    d_currentProduction(0)
{}                        
                          
inline void Rules::clearLocations()
{
    d_location.clear();
}

inline void Rules::setExpectedConflicts(size_t value)
{
    s_nExpectedConflicts = value;
}

inline Terminal const *Rules::eofTerminal()
{
    return &s_eofTerminal;
}

inline Terminal const *Rules::errorTerminal()
{
    return &s_errorTerminal;
}

inline size_t Rules::acceptProductionNr()
{
    return s_acceptProductionNr;
}

inline size_t Rules::expectedConflicts()
{
    return s_nExpectedConflicts;
}

inline Production const &Rules::lastProduction() const
{
    return *d_currentProduction;
}

inline std::string const &Rules::name() const
{
    return d_currentRule->name();
}

inline std::string const &Rules::sType() const  
{                                 
                                  
    return d_currentRule->sType(); 
}

inline size_t Rules::nProductions() const
{
    return d_currentRule->nProductions();
}

inline bool Rules::hasRules() const
{
    return d_currentRule;
}

inline size_t Rules::nElements() const
{
    return d_currentProduction->size();
}

inline void Rules::setLastTerminalValue(size_t value)
{
    d_terminal.back()->setValue(value);
}

inline void Rules::setLastPrecedence(size_t value)
{
    d_terminal.back()->setPrecedence(value);
}

inline void Rules::setStartRule(std::string const &start)
{
    d_startRule = start;
}

inline std::string const &Rules::startRule() const
{
    return d_startRule;
}

inline Production const *Rules::startProduction()
{
    return d_currentProduction;
}

inline Symbol const *Rules::startSymbol() 
{
    return s_startSymbol;
}

inline std::vector<NonTerminal const *> const &Rules::nonTerminals() const
{
    void const *vp = &d_nonTerminal;
    return *reinterpret_cast<std::vector<NonTerminal const *> const *>
           (vp);
}

inline std::vector<Terminal const *> const &Rules::terminals() const
{
    void const *vp = &d_terminal;
    return *reinterpret_cast<std::vector<Terminal const *> const *>(vp);
}

inline std::vector<Production const *> const &Rules::productions() const
{
    void const *vp = &d_production;
    return *reinterpret_cast<std::vector<Production const *> const *>(vp);
}

inline Rules::FileInfo const &Rules::fileInfo(NonTerminal const *nt) const
{
    return d_location.find(nt)->second;
}

inline Symbol const *Rules::symbol(size_t idx) const
{
    return d_currentProduction->rhs(idx - 1);
}

inline bool Rules::empty() const        // true: production rule w/o symbols
{
    return d_empty;
}
