inline LookaheadSet const &RRData::lookaheadSet() const
{
    return d_laSet;
}

inline bool RRData::empty() const
{
    return d_laSet.empty();
}

inline size_t RRData::size() const
{
    return d_laSet.fullSize();
}

inline void RRData::setIdx(size_t reduce)
{
    d_idx = reduce;
}

inline size_t RRData::reduceIdx() const
{
    return d_idx;
}

inline size_t RRData::keepIdx() const
{
    return d_kept;
}

inline bool RRData::isForced(RRData const &rrData)
{
    return rrData.d_forced;
}
