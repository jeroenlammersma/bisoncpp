// $insert inlineLexFunction
inline int Scanner::lex()
{
    return lex_();
}

inline void Scanner::preCode() 
{
}

inline void Scanner::postCode([[maybe_unused]] PostEnum_  type)
{
}

inline void Scanner::print() 
{
    print_();
}

inline Block &Scanner::block()
{
    return d_block;
}

inline void Scanner::clearBlock()
{
    d_block.clear();
}

inline size_t Scanner::number() const
{
    return d_number;
}

inline bool Scanner::hasBlock() const
{
    return not d_block.empty();
}

inline void Scanner::beginTypeSpec()
{
    begin(StartCondition_::typespec);
}
