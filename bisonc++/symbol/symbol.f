inline std::string const &Symbol::name() const
{
    return d_name;
}

inline bool Symbol::isTerminal() const
{
    return !(d_type & NON_TERMINAL);
}

inline bool Symbol::isSymbolic() const
{
    return d_type & SYMBOLIC_TERMINAL;
}

inline bool Symbol::isNonTerminal() const
{
    return d_type & NON_TERMINAL;
}

inline bool Symbol::isUndetermined() const
{
    return d_type == UNDETERMINED;
}

inline bool Symbol::isUsed() const
{
    return d_used;
}

inline bool Symbol::isReserved() const
{
    return d_type & RESERVED;
}

inline void Symbol::setReserved()
{
    d_type |= RESERVED;
}

inline void Symbol::used() const       // d_used is mutable.
{
    d_used = true;
}

inline void Symbol::setType(Type type)
{
    d_type = type;
}

inline void Symbol::setStype(std::string const &stype)
{
    d_stype = stype;
}

inline std::string const &Symbol::sType() const
{
    return d_stype;
}

inline FirstSet const &Symbol::firstSet() const
{
    return v_firstSet();
}
