#include "options.ih"

void Options::setQuotedStrings()
{
    d_arg.option(&d_preInclude, 'H');
    addIncludeQuotes(d_preInclude);

    d_arg.option(&d_scannerInclude, 's');
    addIncludeQuotes(d_scannerInclude);

    d_arg.option(&d_tokenPath, 'F');
    if (not d_tokenPath.empty())                // tokenfile path specified
    {
        addIncludeQuotes(d_tokenPath);
        d_tokenPath = d_tokenPath.substr(1, d_tokenPath.length() - 2);

        d_arg.option(&d_tokenClass, 'K');       // set the token class
        d_arg.option(&d_tokenNameSpace, 'N');   // set the token namespace

        if (d_tokenClass.empty())
            d_tokenClass = s_defaultTokenClass;
    }

}
