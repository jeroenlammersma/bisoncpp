#include "generator.ih"

void Generator::polymorphicSpecializations(ostream &out) const
{
    key(out);

    out.put('\n');
    if (d_options.tagMismatches().value == Options::ON)
        out << "extern char const *idOfTag_[];\n";      // cf. idoftag.cc

    for (auto &poly: d_polymorphic)
    {
        out << 
            "template <>\n"
            "struct TagOf<" << poly.second << ">\n"
            "{\n"
            "    static Tag_ const tag = Tag_::" << poly.first << ";\n"
            "};\n"
            "\n";
    }

    out <<
        "template <>\n"
        "struct TagOf<EndPolyType_>\n"
        "{\n"
        "    static Tag_ const tag = Tag_::END_TAG_;\n"
        "};\n"
        "\n";

    for (auto &poly: d_polymorphic)
    {
        out << 
            "template <>\n"
            "struct TypeOf<Tag_::" << poly.first << ">\n"
            "{\n"
            "    using type = " << poly.second << ";\n"
            "};\n"
            "\n";
    }

    out <<
        "template <>\n"
        "struct TypeOf<Tag_::END_TAG_>\n"
        "{\n"
        "    using type = EndPolyType_;\n"
        "};\n"
        "\n";
}
