#include "generator.ih"

Generator::Map Generator::s_insert =
{
    {"LTYPE",                       &Generator::ltype},
    {"LTYPEclear",                  &Generator::ltypeClear},
    {"LTYPEdata",                   &Generator::ltypeData},
    {"LTYPEpop",                    &Generator::ltypePop},
    {"LTYPEpush",                   &Generator::ltypePush},
    {"LTYPEresize",                 &Generator::ltypeResize},
    {"LTYPEstack",                  &Generator::ltypeStack},
    {"STYPE",                       &Generator::stype},
    {"actioncases",                 &Generator::actionCases},
    {"baseclass",                   &Generator::baseClass},
    {"baseclasscode",               &Generator::baseClassCode},
    {"class.h",                     &Generator::classH},
    {"class.ih",                    &Generator::classIH},
    {"debug",                       &Generator::debug},
    {"debugdecl",                   &Generator::debugDecl},
    {"debugfunctions",              &Generator::debugFunctions},
    {"debugincludes",               &Generator::debugIncludes},
    {"debuglookup",                 &Generator::debugLookup},
    {"errorverbose",                &Generator::errorVerbose},
    {"executeactioncases",          &Generator::executeActionCases},
    {"idoftag",                     &Generator::idOfTag},
    {"lex",                         &Generator::lex},
    {"namespace-close",             &Generator::namespaceClose},
    {"namespace-open",              &Generator::namespaceOpen},
    {"namespace-use",               &Generator::namespaceUse},
    {"notokens",                    &Generator::notokens},
    {"parserbase",                  &Generator::parserBase},
    {"polyincludes",                &Generator::polyIncludes},
    {"polymorphic",                 &Generator::polymorphic},
    {"polymorphicCode",             &Generator::polymorphicCode},
    {"polymorphicOpAssignDecl",     &Generator::polymorphicOpAssignDecl},
    {"polymorphicOpAssignImpl",     &Generator::polymorphicOpAssignImpl},
    {"polymorphicSpecializations",  &Generator::polymorphicSpecializations},
    {"preincludes",                 &Generator::preIncludes},
    {"print",                       &Generator::print},
    {"prompt",                      &Generator::prompt},
    {"scanner.h",                   &Generator::scannerH},
    {"scannerobject",               &Generator::scannerObject},
    {"staticdata",                  &Generator::staticData},
    {"tokens",                      &Generator::tokens},
    {"undefparser",                 &Generator::undefparser},
    {"warnTagMismatches",           &Generator::warnTagMismatches},
};

vector<Generator::AtBool> Generator::s_atBol =
{
    AtBool("@insert-stype", &Generator::ifInsertStype),
    AtBool("@printtokens",  &Generator::ifPrintTokens),
    AtBool("@ltype",        &Generator::ifLtype),
    AtBool("@else",         &Generator::atElse),
    AtBool("@end",          &Generator::atEnd),
};

char const *Generator::s_atFlag = "\\@";

vector<Generator::At> Generator::s_at =
{
    At("\\@tokenfunction",          &Generator::atTokenFunction),
    At("\\@matchedtextfunction",    &Generator::atMatchedTextFunction),
    At("\\@ltype",                  &Generator::atLtype),
    At("\\@$",                      &Generator::atNameSpacedClassname),
    At("\\@",                       &Generator::atClassname),
};
