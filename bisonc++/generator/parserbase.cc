#include "generator.ih"

void Generator::parserBase(ostream &out) const
{
    key(out);

    out << "class " << d_options.className() << "Base";

    if (d_options.useTokenPath())
    {
        out << ": public ";

        if (
            string const &ns = d_options.tokenNameSpace();
            not ns.empty() and ns != d_options.nameSpace()
        )
            out << ns << "::";

        out << d_options.tokenClass();
    }
    out.put('\n');
}



