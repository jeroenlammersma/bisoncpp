
inline std::set<Element const *> const &FirstSet::set() const
{
    return *this;
}

inline FirstSet::FirstSet(Element const **begin, Element const **end)
:
    Baseclass(begin, end),
    d_epsilon(false)
{}

inline FirstSet::FirstSet()
:
    d_epsilon(false)
{}

inline size_t FirstSet::setSize() const
{
    return size() + d_epsilon;
}

inline void FirstSet::rmEpsilon()
{
    d_epsilon = false;
}

inline void FirstSet::addEpsilon()
{
    d_epsilon = true;
}

inline bool FirstSet::hasEpsilon() const
{
    return d_epsilon;
}

inline bool FirstSet::empty() const
{
    return !d_epsilon && Baseclass::empty();
}

inline std::ostream &operator<<(std::ostream &out, FirstSet const &firstSet)
{
    return firstSet.insert(out);
}
