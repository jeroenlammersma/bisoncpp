
inline StateType::StateType(int type)
:
    d_type(type)
{}

inline int StateType::type() const
{
    return d_type;
}

inline void StateType::setType(Type type)
{
    d_type |= type;
}

inline char const *StateType::typeName(int type)
{
    return s_stateName[type & s_mask];
}
